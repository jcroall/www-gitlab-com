---
layout: markdown_page
title: "Building applications that meet common regulatory compliance standards"
---
## Compliance without friction

GitLab helps teams achieve and demonstrate compliance with their specific IT controls.

1. Auditing, logging, traceability and reporting
1. Visible code reviews
1. Merge approvals (change approvals)
1. Access control and granular permissions
2. Masking of protected variables
1. Controlled / protected pipelines
1. Security scanning and License Management for every commit
   1. SAST
   1. DAST
   1. Container
   1. Dependency
1. Security dashboard - track and manage vulnerabilities across projects in one place

See how GitLab helps with specific compliance and framework requirements:
   * [PCI Compliance](../pci-compliance)
   * [GDPR](../../gdpr)  
   * [IEC 62304:2006](../iec-62304)
   * [ISO 13485:2016](../iso-13485)

THE INFORMATION PROVIDED ON THIS WEBSITE IS TO BE USED FOR INFORMATIONAL PURPOSES ONLY. THE INFORMATION SHOULD NOT BE RELIED UPON OR CONSTRUED AS LEGAL OR COMPLIANCE ADVICE OR OPINIONS. THE INFORMATION IS NOT COMPREHENSIVE AND WILL NOT GUARANTEE COMPLIANCE WITH ANY REGULATION OR INDUSTRY STANDARD. YOU MUST NOT RELY ON THE INFORMATION FOUND ON THIS WEBSITE AS AN ALTERNATIVE TO SEEKING PROFESSIONAL ADVICE FROM YOUR ATTORNEY AND/OR COMPLIANCE PROFESSIONAL.
